import React, {useState, useRef} from 'react';
import Thrash from '../icons/Thrash';

import '../App.css'

import {
    CSSTransition,
    TransitionGroup,
} from 'react-transition-group';

function Todo() {
    const [newDeal, setNewDeal] = useState('')

    const [todo, setTodo] = useState([
        {
            id: 1,
            text: 'first deal',
        },
    ]);
    const todos = todo.map((item) => {
        return (
            <CSSTransition
                key={item.id}
                timeout={300}
                classNames="item"
            >
                <div className="border-2 border-blue mt-4 p-2 rounded flex justify-between" key={item.id}>
                    {item.text}
                    <div onClick={() => deleteItem(item.id)} className="hover:cursor-pointer">
                        <Thrash/>
                    </div>
                </div>
            </CSSTransition>
        )
    })


    function addDeal() {
        const id = todo[todo.length - 1]?.id + 1 | 0;
        setTodo([...todo, {id, text: newDeal}])
        setNewDeal('')
    }

    function deleteItem(id: number) {
        const filteredTodo = todo.filter((item) => item.id !== id)
        setTodo(filteredTodo);
    }

    function handleInput(e: React.ChangeEvent<HTMLInputElement>): void {
        setNewDeal(e.target.value);
    }

    function onKeyDown(e: React.KeyboardEvent) {
        if (e.code === 'Enter') {
            addDeal()
        }
    }

    return (
        <div className="flex justify-center">
            <div className="w-3/12 flex flex-col">
                <h1 className="text-3xl font-bold text-center mb-8"> TODO</h1>
                <div className="flex">
                    <input
                        className="shadow appearance-none border w-full rounded py-2 px-3 text-gray-700
                    leading-tight focus:outline-none focus:shadow-outline"
                        id="username" type="text" onChange={handleInput} value={newDeal} onKeyDown={onKeyDown}/>
                    <button onClick={addDeal}
                            disabled={!newDeal}
                            className="bg-blue-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"> add
                    </button>
                </div>
                <div>
                    <TransitionGroup className="todo-list">
                        {todos}
                    </TransitionGroup>
                </div>
            </div>
        </div>
    )
}

export default Todo;
